# Map Markers Generater - (ReactJS)

Map Marker Generator is a small tool that helps creating dummy marker data to import when testing map related functionality and features.

![Screenshot](screenshot/screenshot.gif)

## Getting Started

View [demo](https://gummi-io.github.io/map-markers-generater-reactjs)

## Exploring

If you are interesting playing around with the code, you can clone the repo with following:

```
$ git clone https://github.com/gummi-io/map-markers-generater-reactjs
$ yarn install
$ cp .env.example .env
```

Add your google map api key in the .env file. and run

```
$ yarn watch
```

Project is using Laravel-Mix with browser sync, you can now go to http://localhost:3000

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for detailss
