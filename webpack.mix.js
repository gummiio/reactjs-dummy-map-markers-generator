let mix = require('laravel-mix');
let path = require('path');

mix.setPublicPath('docs')
    .react('src/js/index.js', 'docs/js/')
    .sass('src/scss/index.scss', 'docs/css/')
    .copy('src/index.html', 'docs/index.html')
    .browserSync({
        open: false,
        proxy: false,
        server: {
            baseDir: "docs",
            index: "index.html"
        },
        files: [
            'docs/**/*.html',
            'docs/**/*.css',
            'docs/**/*.js',
        ],
    })
    .extract([
        'react', 'react-dom',
        'react-router', 'react-router-dom',
        'redux', 'react-redux', 'redux-thunk',
        'sortablejs', 'react-sortablejs',
        'faker', 'lodash', 'autosize', 'fetch-jsonp', 'json2csv',
    ])
    .webpackConfig({
        resolve: {
            alias: {
                '@src': path.resolve(__dirname, 'src/js')
            }
        }
    })
