import React, { Component } from 'react';
import { connect } from 'react-redux';
import AppHeader from '@src/components/AppHeader';
import GeneratorSidebar from '@src/components/GeneratorSidebar';
import GoogleMap from '@src/components/GoogleMap';
import * as getters from '@src/store/getters';
import { clearMarkers, deleteMarkers } from '@src/store/actions';
import {
    injectGoogleMapScript,
    fetchUserLocation,
    generateMarkers
} from '@src/store/async-actions';

class Generator extends Component {
    componentDidMount() {
        this.props.injectGoogleMapScript();
        this.props.fetchUserLocation();
    }

    render() {
        const { markers, googleMapReadied, selectedMarkers, canGenerateMarker } = this.props; // state
        const { clearMarkers, deleteMarkers, generateMarkers } = this.props; // action

        const clearButton = ! selectedMarkers.length? (
            <button onClick={clearMarkers} className="button small grey" disabled={! markers.length}>Clear</button>
        ) : '';

        const deleteButton = selectedMarkers.length? (
            <button onClick={deleteMarkers} className="button small red">Delete</button>
        ) : '';

        const mapLoading = ! googleMapReadied? (
            <div className="gogle-map-loading">
                <span>Loading Google Map...</span>
            </div>
        ) : '';

        const map = googleMapReadied? (
            <GoogleMap />
        ) : '';

        return (
            <div className="app-page generator-page">
                <AppHeader />

                <div className="app-container generator-container">
                    <div className="app-sidebar generator-sidebar">
                        <GeneratorSidebar />

                        <div className="app-sidebar-actions generator-actions">
                            { clearButton }
                            { deleteButton }
                            <button onClick={generateMarkers} className="button small" disabled={! canGenerateMarker}>Generate</button>
                        </div>
                    </div>

                    <div className="app-content generator-content">
                        { mapLoading }
                        { map }
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        markers: state.markers,
        googleMapReadied: getters.googleMapReadied(state),
        selectedMarkers: getters.selectedMarkers(state),
        canGenerateMarker: getters.canGenerateMarker(state)
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        clearMarkers(e) {
            e.preventDefault();
            dispatch(clearMarkers())
        },
        deleteMarkers(e) {
            e.preventDefault();
            dispatch(deleteMarkers())
        },
        generateMarkers(e) {
            e.preventDefault();
            dispatch(generateMarkers())
        },
        injectGoogleMapScript() {
            dispatch(injectGoogleMapScript())
        },
        fetchUserLocation() {
            dispatch(fetchUserLocation())
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Generator)
