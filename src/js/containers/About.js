import React from 'react';
import AppHeader from '@src/components/AppHeader';

const fakerRandomName = '`faker[{{name.lastName}}, {{name.firstName}} {{name.suffix}}]`';
const fakerRandomImage = '`faker[{{image.cats}}]`';
const fakerRandomNumberText = '`faker[{{random.number({"max":60})}}]` years old';

export default () => (
    <div className="app-page about-page">
        <AppHeader />

        <div className="app-container about-container">
            <div className="app-sidebar about-sidebar"></div>

            <div className="app-content about-content">
                <div className="about-inner-content">
                    <h2>What is this?</h2>
                    <p>Map Marker Generator is a small tool that helps creating dummy marker data to import when testing map related functionality and features.</p>
                    <p>For example, say that you are building a website that has a store locator with category filter but you don't have any live content yet. In order for you to test the filter, you'd have to manually create dummy stores via the cms one by one. With Map Marker Generator, you can generate some numbers of desired fake stores, with any random custom store meta if needed, and import into the cms.</p>
                    <p>Not only does Map Marker Generator provides testing data for developers, it can also be used for a import sheet guildline for the clients to fill in data. All you need to do is export the desired structure in CSV, and import into Google Sheet or Microsoft Excel.</p>

                    <hr />

                    <h2>Data Fields Value</h2>
                    <p>You can think of Data Fields as meta for your stores, like contact number, address, email, or website url etc... Map Marker Generator is using faker library behind the scene to generate dynamic data. Here are 4 types of format that you can put:</p>

                    <h4 className="small-padd">1. Literal Value: </h4>
                    <p className="small-padd">A fixed hard coded value for all the generated markers. </p>
                    <div className="sample">
                        <label>Fixed String: <input type="text" value="some fixed value" readOnly /></label>
                    </div>

                    <h4 className="small-padd">2. Marker Coordinates: </h4>
                    <p className="small-padd">Special expression that will return generated marker's coordinates</p>
                    <div className="sample">
                        <label>Latitude: <input type="text" value="::latitude" readOnly /></label>
                        <label>Latitude: <input type="text" value="::longitude" readOnly /></label>
                    </div>

                    <h4 className="small-padd">3. Random Value(s): </h4>
                    <p className="small-padd">Special expression that will return one or more items for within the square brackets list. Each items are seperated by pipe (|). Optionally, followed by a colon with a number following after the closing square bracket to define how many items to grab from the list (default is 1). Adding another number to set a range. The special expression is formated as <code>`[item|item|item]:num,num`</code>. </p>
                    <p><b><small>Note the special expression are wrapped between backtick (`)</small></b></p>

                    <div className="sample">
                        <label>1 random value: <input type="text" value="`[Category A|Category B|Category C]`" readOnly /></label>
                        <label>3 random values: <input type="text" value="`[A|B|C|D|E|F|G|H]:3`" readOnly /></label>
                        <label>3-6 random values: <input type="text" value="`[A|B|C|D|E|F|G|H]:3,6`" readOnly /></label>
                        <label>Random value with fixed text: <input type="text" value="`[18-|19-30|31-50|51-65|65+]` years old" readOnly /></label>
                    </div>

                    <h4 className="small-padd">4. Faker Generated: </h4>
                    <p className="small-padd">Special expression that will use <a href="https://github.com/marak/Faker.js/#fakerfake" target="_blank" rel="noopener">Faker</a> library to generate faker value using the fake() method. The special expression is formated as <code>`faker[ the faker string ]`</code>.</p>
                    <p><b><small>Note the special expression are wrapped between backtick (`)</small></b></p>

                    <div className="sample">
                        <label>Faker name: <input type="text" value={fakerRandomName} readOnly /></label>
                        <label>Faker image: <input type="text" value={fakerRandomImage} readOnly /></label>
                        <label>Faker number with fixed text: <input type="text" value={fakerRandomNumberText} readOnly /></label>
                        <label>Faker value with fixed text: <input type="text" value="`[18-|19-30|31-50|51-65|65+]` years old" readOnly /></label>
                    </div>
                </div>
            </div>
        </div>
    </div>
)
