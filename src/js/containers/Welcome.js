import React from 'react';

export default ({ history }) => (
    <div className="welcome-page">
        <div className="content">
            <h1>Map Markers Generator <span className="app-type">-ReactJS</span></h1>

            <p>Map Markers Generator allows you to generates some random markers on the map, with custom attributes. This tool is useful if you are building some store locator or Google map interaction app. Before you have the real data, you can use the generated markers to do some functional testing such as radius search or filtering. </p>

            <p>This app is for educational purposes for by using ReactJS with simple Redux, Thunk and React-Router integration. </p>

            <div className="button-holder">
                <a className="button" onClick={() => history.push('generator')}>Start Generating</a>
            </div>
        </div>
    </div>
)
