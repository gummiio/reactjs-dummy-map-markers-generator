import React, { Component } from 'react';
import { connect } from 'react-redux';
import json2csv from 'json2csv';
import * as getters from '@src/store/getters';
import AppHeader from '@src/components/AppHeader';
import ExportSidebar from '@src/components/ExportSidebar';

class Export extends Component {
    constructor(props) {
        super(props);

        this.state = {
            selected: false
        }

        this.output = this.output.bind(this);
        this.extension = this.extension.bind(this);
        this.selectAll = this.selectAll.bind(this);
        this.returnJson = this.returnJson.bind(this);
        this.returnCsv = this.returnCsv.bind(this);
        this.downlaodExportFile = this.downlaodExportFile.bind(this);
    }


    output() {
        return this.props.exportActiveTab == 'JSON'? this.returnJson() : this.returnCsv();
    }

    extension() {
        return this.props.exportActiveTab == 'JSON'? '.json' : '.csv';
    }

    selectAll() {
        this.setState({selected : ! this.state.selected});

        if (! this.state.selected) {
            return;
        }

        window.getSelection().selectAllChildren(this.preOutput);
    }

    returnJson() {
        return JSON.stringify(this.props.markersWithData, null, this.props.exportJsonSpaces);
    }

    returnCsv() {
        return json2csv({
            data: this.props.markersWithData,
            fields: this.props.dataFieldKeys,
            hasCSVColumnTitle : this.props.exportCsvTitle,
            del : this.props.exportCsvDelimiter,
            newLine: this.props.exportCsvNewLine
        });
    }

    downlaodExportFile(e) {
        e.preventDefault();
        const fileName = "marker-generater-export-data";

        const link = document.createElement("a");
        link.href = 'data:text/csv;charset=utf-8,' + escape(this.output());
        link.style = "visibility:hidden";
        link.download = fileName + this.extension();

        this.$el.appendChild(link);
        link.click();
        this.$el.removeChild(link);
    }

    render() {
        const { markers, markersWithData } = this.props;

        return (
            <div className="app-page export-page" ref={(e) => this.$el = e}>
                <AppHeader />

                <div className="app-container export-container">
                    <div className="app-sidebar export-sidebar">
                        <ExportSidebar />

                        <div className="app-sidebar-actions export-actions">
                            <button
                                onClick={this.downlaodExportFile}
                                className="button small"
                                disabled={! markers.length}
                            >Download</button>
                        </div>
                    </div>

                    <div className="app-content export-content">
                        {
                            markersWithData.length == 0 &&
                            <div className="padded-2x">No markers has been generated yet.</div>
                        }

                        {
                            markersWithData.length > 0 &&
                            <pre onClick={this.selectAll} ref={(e) => this.preOutput = e}>{ this.output() }</pre>
                        }
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        exportActiveTab: state.exportActiveTab,
        markers: state.markers,
        exportJsonSpaces: parseInt(state.exportJsonSpaces),
        exportCsvTitle: !! state.exportCsvTitle,
        exportCsvDelimiter: state.exportCsvDelimiter,
        exportCsvNewLine: state.exportCsvNewLine,
        markersWithData: getters.markersWithData(state),
        dataFieldKeys: getters.dataFieldKeys(state),
    }
}

export default connect(mapStateToProps, null)(Export)
