import Welcome from '@src/containers/Welcome'
import About from '@src/containers/About'
import Generator from '@src/containers/Generator'
import Export from '@src/containers/Export'

export default [
    {
        path: '/',
        component: Welcome
    },
    {
        path: '/about',
        component: About
    },
    {
        path: '/generator',
        component: Generator
    },
    {
        path: '/export',
        component: Export
    },
]
