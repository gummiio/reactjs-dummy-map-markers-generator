import GoogleMap from '@src/services/GoogleMap';
import parseFieldValue from '@src/helpers/fieldParser';
import fetchJsonp from 'fetch-jsonp';
import * as getters from '@src/store/getters';
import {
    addDataField as addDataFieldAction,
    googleScriptLoaded,
    updateUserLocation,
    updateMapInfo as updateMapInfoAction,
    toggleMapMovement,
    addMarkers,
    toggleMarker as toggleMarkerAction,
    reorderDataField,
} from '@src/store/actions';

export const addDataField = (targetNode) => {
    return (dispatch, getState) => {
        dispatch(addDataFieldAction());

        return new Promise((resolve) => {
            var observer = new MutationObserver((mutations) => {
                resolve(mutations)
            });

            observer.observe(targetNode, { childList: true });
        })
    };
}

export const injectGoogleMapScript = () => {
    return (dispatch, getState) => {
        if (getState().googleScriptLoaded) {
            return;
        }

        GoogleMap.injectScript(() => dispatch(googleScriptLoaded()));
    };
}

export const fetchUserLocation = () => {
    return (dispatch, getState) => {
        if (getters.userLocated(getState())) {
            return;
        }

        fetchJsonp('https://geoip-db.com/jsonp', { jsonpCallbackFunction: 'callback' })
            .then((re) => re.json())
            .then(({ latitude:lat, longitude:lng }) => {
                dispatch(updateUserLocation({ lat, lng }));
            })
            .catch((error) => {
                dispatch(updateUserLocation({ lat: 0, lng: 0 }));
            });
    };
}

export const updateMapInfo = () => {
    return (dispatch, getState) => {
        const map = GoogleMap.map;

        dispatch(updateMapInfoAction({
            zoom: map.getZoom(),
            lat: map.getCenter().lat(),
            lng: map.getCenter().lng()
        }));

        dispatch(toggleMapMovement(false));
    }
}

export const generateMarkers = () => {
    return (dispatch, getState) => {
        if (! getters.canGenerateMarker(getState())) {
            return false;
        }

        let { minLat, maxLat, minLng, maxLng } = GoogleMap.getBoundInfo();
        let markers = []

        for (let i = 0; i < getState().markersLimit; i ++) {
            const lat = _.random(minLat, maxLat, 14);
            const lng = _.random(minLng, maxLng, 14);
            // const key = `${lat}:${lng}`.replace(/\W/g, '');
            const marker = { lat, lng, selected: false, data: {} };

            getState().dataFields.forEach(({ name, value }) => marker.data[name] = parseFieldValue(value, marker));

            markers.push(marker);
        }

        dispatch(addMarkers(markers));
    }
}

export const toggleMarker = (targetMarker) => {
    return (dispatch) => {
        dispatch(toggleMarkerAction(targetMarker));
        GoogleMap.toggleMarker(targetMarker);
    }
}


export const reorderDataFieldByIndex = (newIndexes) => {
    return (dispatch, getState) => {
        const dataFields = getState().dataFields;
        const newFields = newIndexes.map(i => dataFields[i]);

        dispatch(reorderDataField(newFields));
    }
}
