export const markersWithData = (state) => {
    return state.markers.map(({ data }) => data);
}

export const dataFieldKeys = (state) => {
    return state.dataFields.map(({ name }) => name);
}

export const selectedMarkers = (state) => {
    return state.markers.filter(m => m.selected);
}

export const userLocated = (state) => {
    const { lat, lng } = state.userLocation;
    return lat !== null && lng !== null;
}

export const markersLimitError = (state) => {
    if (state.markersLimit < 0 || state.markersLimit > 100) {
        return 'Limit must set between 1 ~ 100';
    }

    return null;
}

export const googleMapReadied = (state) => {
    return state.googleScriptLoaded && userLocated(state);
}

export const canGenerateMarker = (state) => {
    return googleMapReadied(state) && ! markersLimitError(state) && ! state.mapMoving;
}
