export default {
    generatorActiveTab: 'Map',
    exportActiveTab: 'JSON',
    markersLimit: 20,
    dataFields: [
        { name: 'lng', value: '::latitude' },
        { name: 'lat', value: '::longitude' },
        { name: 'Company', value: '`faker[{{company.companyName}}]`' },
        { name: 'Owner', value: '`faker[{{name.firstName}}, {{name.lastName}}]`' },
        { name: 'Rating', value: '`[1|2|3|4|5]`' },
    ],
    markers: [],
    googleScriptLoaded: false,
    userLocation: {
        lat: null,
        lng: null
    },
    mapInfo: {
        zoom: null,
        lat: null,
        lng: null
    },
    mapMoving: false,
    exportJsonSpaces: 2,
    exportCsvTitle: true,
    exportCsvDelimiter: ','
}
