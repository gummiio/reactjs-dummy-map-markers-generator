export default (initialState = {}) => {
    return (state = initialState, action) => {
        switch (action.type) {
            case 'switchGeneratorTab':
                return {
                    ...state,
                    generatorActiveTab: action.tab
                };

            case 'switchExportTab':
                return {
                    ...state,
                    exportActiveTab: action.tab
                };

            case 'updateMarkersLimit':
                return {
                    ...state,
                    markersLimit: action.limit
                };

            case 'addDataField':
                return {
                    ...state,
                    dataFields: [
                        ...state.dataFields,
                        action.field
                    ]
                };

            case 'updateDataFieldName':
                return {
                    ...state,
                    dataFields: state.dataFields.map(field => {
                        return field == action.field?
                            {...field, name: action.name} : field;
                    })
                }

            case 'updateDataFieldValue':
                return {
                    ...state,
                    dataFields: state.dataFields.map(field => {
                        return field == action.field?
                            {...field, value: action.value} : field;
                    })
                }

            case 'removeDataField':
                return {
                    ...state,
                    dataFields: state.dataFields.filter(field => field != action.field)
                }

            case 'reorderDataField':
                return {
                    ...state,
                    dataFields: action.newFields
                }

            case 'addMarkers':
                return {
                    ...state,
                    markers: state.markers.concat(action.newMarkers)
                };

            case 'toggleMarker':
                return {
                    ...state,
                    markers: state.markers.map(marker => {
                        return marker == action.marker?
                            {...marker, selected: ! marker.selected} : marker;
                    })
                }

            case 'clearMarkers':
                return {
                    ...state,
                    markers: []
                }

            case 'deleteMarkers':
                return {
                    ...state,
                    markers: state.markers.filter(marker => ! marker.selected)
                };

            case 'googleScriptLoaded':
                return {
                    ...state,
                    googleScriptLoaded: true
                };

            case 'updateUserLocation':
                return {
                    ...state,
                    userLocation: action.location
                };

            case 'updateMapInfo':
                return {
                    ...state,
                    mapInfo: action.info
                };

            case 'toggleMapMovement':
                return {
                    ...state,
                    mapMoving: action.status
                };

            case 'updateExportJsonSpaces':
                return {
                    ...state,
                    exportJsonSpaces: action.spaces
                };

            case 'updateExportCsvTitle':
                return {
                    ...state,
                    exportCsvTitle: action.withTitle
                };

            case 'updateExportCsvDelimiter':
                return {
                    ...state,
                    exportCsvDelimiter: action.delimiter
                };

            default:
                return state
        }
    }
}
