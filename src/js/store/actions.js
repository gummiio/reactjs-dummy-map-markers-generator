export function switchGeneratorTab(tab) {
    return {
        type: 'switchGeneratorTab',
        tab
    };
};

export function switchExportTab(tab) {
    return {
        type: 'switchExportTab',
        tab
    };
};

export function updateMarkersLimit(limit) {
    return {
        type: 'updateMarkersLimit',
        limit
    };
};

export function addDataField() {
    return {
        type: 'addDataField',
        field: { name: '', value: '' }
    };
};

export function updateDataFieldName({ field, name }) {
    return {
        type: 'updateDataFieldName',
        field,
        name
    };
};

export function updateDataFieldValue({ field, value }) {
    return {
        type: 'updateDataFieldValue',
        field,
        value
    };
};

export function removeDataField(field) {
    return {
        type: 'removeDataField',
        field
    };
};

export function reorderDataField(newFields) {
    return {
        type: 'reorderDataField',
        newFields
    };
};

export function addMarkers(newMarkers) {
    return {
        type: 'addMarkers',
        newMarkers
    };
};

export function toggleMarker(marker) {
    return {
        type: 'toggleMarker',
        marker
    };
};

export function clearMarkers() {
    return {
        type: 'clearMarkers'
    };
};

export function deleteMarkers(state) {
    return {
        type: 'deleteMarkers'
    };
};

export function googleScriptLoaded() {
    return {
        type: 'googleScriptLoaded'
    };
};

export function updateUserLocation(location) {
    return {
        type: 'updateUserLocation',
        location
    };
};

export function updateMapInfo(info) {
    return {
        type: 'updateMapInfo',
        info
    };
};

export function toggleMapMovement(status = true) {
    return {
        type: 'toggleMapMovement',
        status: !! status
    };
};

export function updateExportJsonSpaces(spaces) {
    return {
        type: 'updateExportJsonSpaces',
        spaces
    };
};

export function updateExportCsvTitle(withTitle) {
    return {
        type: 'updateExportCsvTitle',
        withTitle
    };
};

export function updateExportCsvDelimiter(delimiter) {
    return {
        type: 'updateExportCsvDelimiter',
        delimiter
    };
};
