import React, { Component } from 'react';
import Tab from '@src/components/Tab';

class Tabs extends Component {
    constructor(props) {
        super(props);

        this.selectTab = this.selectTab.bind(this);
        this.tabClasses = this.tabClasses.bind(this);
        this.isActiveTab = this.isActiveTab.bind(this);
    }

    selectTab(e, selectedTab) {
        this.props.tabSwitched(selectedTab.props.label);
    }

    tabClasses(tab) {
        return this.isActiveTab(tab)? 'is-current' : '';
    }

    isActiveTab(tab) {
        return tab.props.label === this.props.activeTab;
    }

    render() {
        return (
            <div className="tab-container">
                <ul className="tab-tabs">
                    { this.props.children.map((tab, i) => (
                        <li className="tab-tab" key={i}>
                            <a
                                onClick={(e) => this.selectTab(e, tab)}
                                className={this.tabClasses(tab)}
                            >
                                <div>
                                    <i className={'fa ' + tab.props.icon}>
                                        {
                                            tab.props.badge > 0 &&
                                            <span className="badge">{ tab.props.badge }</span>
                                        }
                                    </i>
                                </div>
                                <span>{ tab.props.label }</span>
                            </a>
                        </li>
                    )) }
                </ul>

                <div className="tab-contents">
                    { this.props.children.map((child, i) => (
                        <Tab {...child.props} isActive={this.isActiveTab(child)} key={i} />
                    )) }
                </div>
            </div>
        )
    }
}

export default Tabs;
