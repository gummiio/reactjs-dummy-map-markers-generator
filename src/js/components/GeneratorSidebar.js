import React from 'react';
import { connect } from 'react-redux';
import { switchGeneratorTab } from '@src/store/actions';
import Tabs from '@src/components/Tabs';
import Tab from '@src/components/Tab';
import GeneratorMapTab from '@src/components/GeneratorMapTab';
import GeneratorDataTab from '@src/components/GeneratorDataTab';
import GeneratorMarkersTab from '@src/components/GeneratorMarkersTab';

const GeneratorSidebar = ({ switchGeneratorTab, generatorActiveTab, markersCount }) => (
    <div className="app-sidebar-inner generator-configurator">
        <Tabs tabSwitched={switchGeneratorTab} activeTab={generatorActiveTab}>
            <Tab icon="fa-map" label="Map">
                <GeneratorMapTab />
            </Tab>

            <Tab icon="fa-list-ul" label="Data">
                <GeneratorDataTab />
            </Tab>

            <Tab icon="fa-map-marker" label="Markers" badge={markersCount}>
                <GeneratorMarkersTab />
            </Tab>
        </Tabs>
    </div>
);

const mapStateToProps = (state) => {
    return {
        generatorActiveTab: state.generatorActiveTab,
        markersCount: state.markers.length,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        switchGeneratorTab(tab) {
            dispatch(switchGeneratorTab(tab))
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(GeneratorSidebar)
