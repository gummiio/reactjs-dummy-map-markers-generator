import React from 'react';
import { connect } from 'react-redux';
import * as getters from '@src/store/getters';
import { updateMarkersLimit } from '@src/store/actions';
import FormControl from '@src/components/FormControl';

const GeneratorMapTab = ({ markersLimit, updateMarkersLimit, markersLimitError }) => (
    <div className="configurator-form">
        <p className="instruction padded">Zoom and move the map to the area that you want to generate the markers within. Enter how many markers you want to generate within the area. (note if your area includes sea, the marker might be on the sea)</p>

        <FormControl label="Number of markers to generate" className="padded">
            <div>
                <input
                    type="number"
                    value={markersLimit}
                    onChange={updateMarkersLimit}
                    max="100"
                    min="1"
                    className={markersLimitError && 'is-invalid'}
                />

                {
                    markersLimitError &&
                    <span className="form-error">{ markersLimitError }</span>
                }
            </div>
        </FormControl>
    </div>
)

const mapStateToProps = (state) => {
    return {
        markersLimit: state.markersLimit,
        markersLimitError: getters.markersLimitError(state)
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        updateMarkersLimit(e) {
            dispatch(updateMarkersLimit(e.target.value))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(GeneratorMapTab)
