import React, { Component } from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import Sortable from 'react-sortablejs';
import { reorderDataFieldByIndex, addDataField } from '@src/store/async-actions';
import DataField from '@src/components/DataField';

class GeneratorDataTab extends Component {

    render() {
        const { fields, reorderDataField, addDataField } = this.props;

        return (
            <div className="configurator-form" ref={(el) => this.$el = el}>
                <p className="instruction padded">Add/Remove the data that need to be generated for each marker. For more information about the available values, please visite  <NavLink exact to="/about">about page</NavLink></p>

                <Sortable
                    onChange={reorderDataField}
                    className="marker-data-holder"
                    ref={(el) => this.fieldsList = el}
                    options={{ handle: '.sort-handle' }}
                >
                    { fields.map((field, i) => (
                        <DataField key={i} data-id={i} field={field}></DataField>
                    )) }
                </Sortable>

                <div className="marker-data-add-field padded">
                    <a href="#" onClick={(e) => addDataField(e, this)} className="add-data">
                        Add Field <i className="fa fa-plus" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        fields: state.dataFields,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        reorderDataField(newIndexes) {
            dispatch(reorderDataFieldByIndex(newIndexes))
        },
        addDataField(e, vm) {
            e.preventDefault();
            const sortableEl = vm.fieldsList.sortable.el;

            dispatch(addDataField(sortableEl))
                .then(() => {
                    const tabContent = vm.$el.parentElement;
                    tabContent.scrollTop = tabContent.scrollHeight;

                    const fields = sortableEl.children;
                    fields[fields.length-1].querySelector("[type='text']").focus();
                });
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(GeneratorDataTab)
