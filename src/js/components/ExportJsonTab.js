import React from 'react';
import { connect } from 'react-redux';
import { updateExportJsonSpaces } from '@src/store/actions';
import FormControl from '@src/components/FormControl';

const ExportJsonTab = ({ exportJsonSpaces, updateExportJsonSpaces }) => (
    <div className="configurator-form">
        <p className="instruction padded"></p>

        <FormControl label="Json tab spaces" className="padded">
            <div>
                <label className="inline-label">
                    <input type="radio" checked={exportJsonSpaces == 2} onChange={updateExportJsonSpaces} value="2" />2 spaces
                </label>

                <label className="inline-label">
                    <input type="radio" checked={exportJsonSpaces == 4} onChange={updateExportJsonSpaces} value="4" />4 spaces
                </label>
            </div>
        </FormControl>
    </div>
);

const mapStateToProps = (state) => {
    return {
        exportJsonSpaces: state.exportJsonSpaces,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        updateExportJsonSpaces(e) {
            dispatch(updateExportJsonSpaces(e.target.value))
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ExportJsonTab)
