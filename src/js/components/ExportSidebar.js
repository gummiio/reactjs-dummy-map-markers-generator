import React from 'react';
import { connect } from 'react-redux';
import { switchExportTab } from '@src/store/actions';
import Tabs from '@src/components/Tabs';
import Tab from '@src/components/Tab';
import ExportJsonTab from '@src/components/ExportJsonTab';
import ExportCsvTab from '@src/components/ExportCsvTab';

const ExportSidebar = ({ switchExportTab, exportActiveTab }) => (
    <div className="app-sidebar-inner generator-configurator">
        <Tabs tabSwitched={switchExportTab} activeTab={exportActiveTab}>
            <Tab icon="fa-code" label="JSON">
                <ExportJsonTab />
            </Tab>

            <Tab icon="fa-file-excel-o" label="CSV">
                <ExportCsvTab />
            </Tab>
        </Tabs>
    </div>
);

const mapStateToProps = (state) => {
    return {
        exportActiveTab: state.exportActiveTab
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        switchExportTab(tab) {
            dispatch(switchExportTab(tab))
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ExportSidebar);
