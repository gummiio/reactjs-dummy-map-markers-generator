import React, { Component } from 'react';

const Tab = ({ children, isActive }) => {
    if (! isActive) return null;

    return (
        <section className="tab-content">
            { children }
        </section>
    )
}

export default Tab;
