import React from 'react';
import { NavLink } from 'react-router-dom';

export default () => (
    <div className="app-header">
        <div className="app-branding">
            <NavLink exact to="/" className="home-link">Map Marker Generator</NavLink>
        </div>

        <div className="app-navigation">
            <ul className="app-menu">
                <li>
                    <NavLink activeClassName="router-link-active" exact to="/about">About</NavLink>
                </li>
                <li>
                    <NavLink activeClassName="router-link-active" exact to="/generator">Generator</NavLink>
                </li>
                <li>
                    <NavLink activeClassName="router-link-active" exact to="/export">Export</NavLink>
                </li>
                <li>
                    <a href="https://github.com/gummi-io/map-markers-generater-reactjs" target="_blank" rel="noopener">
                        <i className="fa fa-github"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
)
