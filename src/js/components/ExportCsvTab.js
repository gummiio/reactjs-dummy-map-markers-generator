import React from 'react';
import { connect } from 'react-redux';
import { updateExportCsvTitle, updateExportCsvDelimiter } from '@src/store/actions';
import FormControl from '@src/components/FormControl';

const ExportCsvTab = ({
    csvTitle,
    csvDelimiter,
    exportJsonSpaces,
    updateExportJsonSpaces,
    updateExportCsvTitle,
    updateExportCsvDelimiter
}) => (
    <div className="configurator-form">
        <p className="instruction padded">Zoom and move the map to the area that you want to generate the markers within. Enter how many markers you want to generate within the area. (note if your area includes sea, the marker might be on the sea)</p>

        <FormControl label="Include field name in first row" className="padded">
            <div>
                <input type="checkbox" checked={csvTitle} onChange={updateExportCsvTitle} />
            </div>
        </FormControl>

        <FormControl label="Delimiter character for each column" className="padded">
            <div>
                <input type="text" value={csvDelimiter} onChange={updateExportCsvDelimiter} />
            </div>
        </FormControl>
    </div>
);

const mapStateToProps = (state) => {
    return {
        csvTitle: state.exportCsvTitle,
        csvDelimiter: state.exportCsvDelimiter,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        updateExportCsvTitle(e) {
            dispatch(updateExportCsvTitle(e.target.checked))
        },
        updateExportCsvDelimiter(e) {
            dispatch(updateExportCsvDelimiter(e.target.value))
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ExportCsvTab)
