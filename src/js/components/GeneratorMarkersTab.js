import React from 'react';
import { connect } from 'react-redux';
import MarkerListItem from '@src/components/MarkerListItem';

const GeneratorMarkersTab = ({ markers }) => (
    <div>
        <p className="instruction padded">List of generated markers.</p>

        <div className="markers-holder">
            { markers.map((marker, i) => (
                <MarkerListItem key={i} marker={marker} />
            )) }
        </div>
    </div>
);

const mapStateToProps = (state) => {
    return {
        markers: state.markers
    }
}

export default connect(mapStateToProps, null)(GeneratorMarkersTab)
