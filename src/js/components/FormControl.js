import React from 'react';

const FormControl = ({ children, label, className, note }) => (
    <div className={'form-control ' + className}>
        <div className="form-label">
            <label>{ label }</label>
            {
                note &&
                <span className="form-note">{ note }</span>
            }
        </div>

        <div className="form-input">
            { children }
        </div>
    </div>
)

export default FormControl;
