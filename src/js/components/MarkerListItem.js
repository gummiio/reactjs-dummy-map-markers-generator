import React, { Component } from 'react';
import { connect } from 'react-redux';
import { toggleMarker } from '@src/store/async-actions';

class GeneratorMarkersTab extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showData: false
        }

        this.toggleShowData = this.toggleShowData.bind(this);
    }

    toggleShowData(e) {
        e.preventDefault();
        e.stopPropagation();

        this.setState({showData: ! this.state.showData});
    }

    render() {
        const { marker, toggleMarker } = this.props;

        return (
            <label className={'generated-marker padded ' + (marker.selected? 'is-selected' : '')}>
                <div className="checkbox">
                    <input
                        type="checkbox"
                        checked={marker.selected}
                        onChange={e => toggleMarker(marker)} />
                </div>

                <div className="value">
                    <div className="marker-location">
                        <div className="coord"><b>Lng:</b> { marker.lng }</div>
                        <div className="coord"><b>Lat:</b> { marker.lat }</div>
                    </div>
                </div>

                <div className="trigger">
                    <a href="#" onClick={this.toggleShowData}>
                        <i className="fa fa-ellipsis-v" aria-hidden="true"></i>
                    </a>
                </div>

                {
                    this.state.showData &&
                    <div className="data padded" v-if="showData">
                        { Object.keys(marker.data).map((name, i) => (
                            <div className="marker-data" key={i}>
                                <b>{ name }</b> <span>{ marker.data[name] }</span>
                            </div>
                        )) }
                    </div>
                }
            </label>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        toggleMarker(marker) {
            dispatch(toggleMarker(marker))
        },
    }
}

export default connect(null, mapDispatchToProps)(GeneratorMarkersTab)
