import React, { Component } from 'react';
import { connect } from 'react-redux';
import GoogleMapService from '@src/services/GoogleMap';
import { toggleMapMovement } from '@src/store/actions';
import { updateMapInfo } from '@src/store/async-actions';

class GoogleMap extends Component {
    componentDidMount() {
        const {
            updateMapInfo,
            toggleMapMovement
        } = this.props;

        GoogleMapService.newMap(this.googleMap)
            .addEvent('bounds_changed', updateMapInfo, true)
            .addEvent('bounds_changed', this.updateMarkers, true)
            .addEvent('dragstart', toggleMapMovement)
            .addEvent('idle', updateMapInfo);
    }

    componentDidUpdate() {
        this.updateMarkers();
    }

    updateMarkers() {
        GoogleMapService.clearMarkers().populateMarkers();
    }

    render() {
        return (
            <div ref={(el) => this.googleMap = el} className="map"></div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        markers: state.markers
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        toggleMapMovement() {
            dispatch(toggleMapMovement())
        },
        updateMapInfo() {
            dispatch(updateMapInfo())
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(GoogleMap)
