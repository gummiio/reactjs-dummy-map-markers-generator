import React, { Component } from 'react';
import { connect } from 'react-redux';
import autosize from 'autosize';
import { updateDataFieldName, updateDataFieldValue, removeDataField } from '@src/store/actions';

class DataField extends Component {
    constructor(props) {
        super(props);

        this.adjustSize = this.adjustSize.bind(this);
    }

    componentDidMount() {
        autosize(this.textarea);
    }

    adjustSize() {
        autosize.update(this.textarea);
    }

    removeField(e) {
        e.preventDefault();
        this.props.removeDataField(this.props.field);
    }

    render() {
        const {
            field,
            updateDataFieldName,
            updateDataFieldValue
        } = this.props;

        return (
            <div className="marker-data-field padded" data-id={this.props['data-id']}>
                <div className="sort-handle">
                    <i className="fa fa-sort" aria-hidden="true"></i>
                </div>

                <div className="field-content">
                    <div className="name">
                        <div className="label">
                            <label>Field Name</label>
                        </div>
                        <div className="control">
                            <input
                                type="text"
                                value={field.name}
                                onChange={(e) => updateDataFieldName(field, e.target.value)}
                            />
                        </div>
                    </div>
                    <div className="value">
                        <div className="label">
                            <label>Field Value</label>
                        </div>
                        <div className="control">
                            <textarea
                                value={field.value}
                                onChange={(e) => updateDataFieldValue(field, e.target.value)}
                                ref={(e) => this.textarea = e}
                                onFocus={this.adjustSize}
                            ></textarea>
                        </div>
                    </div>
                </div>

                <a href="#" onClick={e => this.removeField(e)} className="delete-data">
                    <i className="fa fa-minus-circle" aria-hidden="true"></i>
                </a>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        updateDataFieldName(field, name) {
            dispatch(updateDataFieldName({ field, name }))
        },
        updateDataFieldValue(field, value) {
            dispatch(updateDataFieldValue({ field, value }))
        },
        removeDataField(field) {
            dispatch(removeDataField(field))
        },
    }
}

export default connect(null, mapDispatchToProps)(DataField)
