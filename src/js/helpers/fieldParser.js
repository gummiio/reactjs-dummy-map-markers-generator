import faker from 'faker';
import _ from 'lodash';

const fakerRegExp = /`\s*faker\[([^\]]+)\]\s*`/;
const randomRegExp = /`\s*\[([^\]]+)\](:(\d+)(,(\d+))?)?\s*`/;

const parseFaker = (value) => {
    const matches = value.match(fakerRegExp);
    try {
        return value.replace(matches[0], faker.fake(matches[1]));
    } catch (error) {
        return value;
    }
}

const parseRandom = (value) => {
    const matches = value.match(randomRegExp);
    // console.log(matches);
    const choices = matches[1].split('|');
    let min = _.get(matches, 3, 1);
    let max = _.get(matches, 5, 1);

    if (max > choices.length) {
        max = choices.length;
    }

    if (min < 1) {
        min = 1;
    }

    if (min > max) {
        min = max;
    }

    min = parseInt(min);
    max = parseInt(max);

    const random = _.random(min, max);

    return _.chain(choices).shuffle().slice(0, random).join("|").value();
}

const parseFieldValue = (value, marker) => {
    if (value == '::latitude') {
        return marker.lat;
    }

    if (value == '::longitude') {
        return marker.lng;
    }

    if (fakerRegExp.test(value)) {
        return parseFaker(value);
    }

    if (randomRegExp.test(value)) {
        return parseRandom(value);
    }

    return value;
}

export default parseFieldValue;
