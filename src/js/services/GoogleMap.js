import store from '@src/store';
import mapStyle from '@src/services/mapStyle';
import { toggleMarker } from '@src/store/actions';

const defaultIcon = 'https://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|00b894';
const selectedIcon = 'https://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|e74c3c';

class GoogleMap {
    constructor() {
        this.map = null;
        this.markers = [];
    }

    injectScript(callback) {
        const googleKey = process.env.MIX_GOOGLE_API_KEY;
        const script = window.document.createElement("script");
        script.type = "text/javascript";
        script.src = `https://maps.googleapis.com/maps/api/js?key=${googleKey}&libraries=places`;
        script.onload = callback;
        window.document.head.appendChild(script);
    }

    newMap(selector) {
        const { lat, lng, zoom } = store.getState().mapInfo;
        const { lat:userLat, lng:userLng } = store.getState().userLocation;

        const options = {
            disableDefaultUI: true,
            center: {
                lat: lat || userLat,
                lng: lng || userLng
            },
            zoom: zoom || 12,
            styles: mapStyle
        };

        this.map = new google.maps.Map(selector, options);

        return this;
    }

    clearMarkers() {
        this.markers.forEach(marker => marker.setMap(null));
        this.markers = [];

        return this;
    }

    populateMarkers() {
        store.getState().markers.forEach(marker => {
            const mapMarker = new google.maps.Marker({
                marker: marker,
                position: { lat: marker.lat, lng: marker.lng },
                map: this.map,
                icon: marker.selected? selectedIcon : defaultIcon,
                timeout: null
            });

            mapMarker.addListener('click', (e) => {
                store.dispatch(toggleMarker(marker));
            });

            this.markers.push(mapMarker)
        })

        return this;
    }

    addEvent(name, callback, once = false) {
        const fn = once? 'addListenerOnce' : 'addListener';

        google.maps.event[fn](this.map, name, callback);

        return this;
    }

    toggleMarker(targetMarker) {
        const mapMarker = this.markers.find(m => m.marker == targetMarker);
        mapMarker.setIcon(targetMarker.selected? selectedIcon : defaultIcon);
        mapMarker.setAnimation(google.maps.Animation.BOUNCE);
        clearTimeout(mapMarker.timeout);
        mapMarker.timeout = setTimeout(() => mapMarker.setAnimation(null), 700);
    }

    getBoundInfo() {
        const bounds = this.map.getBounds()

        return {
            minLat: bounds.getSouthWest().lat(),
            maxLat: bounds.getNorthEast().lat(),
            minLng: bounds.getSouthWest().lng(),
            maxLng: bounds.getNorthEast().lng(),
        };
    }
}

const googleMap = new GoogleMap;

export default googleMap;
