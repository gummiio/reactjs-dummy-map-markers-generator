import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter, Route } from 'react-router-dom';
import { Provider } from 'react-redux';
import routes from '@src/router';
import store from '@src/store'

ReactDOM.render(
    <Provider store={store}>
        <HashRouter>
            <div>
                { routes.map((route, i) => (
                    <Route exact path={route.path} key={i} component={route.component} />
                )) }
            </div>
        </HashRouter>
    </Provider>
, document.getElementById('root'));
